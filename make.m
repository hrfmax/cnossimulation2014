% --- make.m
% --- make all compiled componets for distribution
% --- final executable code contained in ./bin/
% --- to install MATLAB MCR: from the matlab prompt:
% ---     mcr_install
% --- 
% --- IMPORTANT: before compiling execute command 'clear classes' and update toolbox cache
% ---
% --- link libstdc if required
% --- sudo ln -s /usr/lib/x86_64-linux-gnu/libstdc++.so.6 /home/tarn/Matlab_2010b/sys/os/glnxa64/libstdc++.so.6
% @author John P. 


clear classes
create_shell_files = 0;

% --- -----------------------------------
% --- make sure running from proper location
% --- -----------------------------------
if ~exist( [pwd '/process_code'], 'dir' )
  disp( 'hmmm . . . no code scripts detected . . .' );
  return;
end;

% --- -----------------------------------
% --- create the compiled binary output directory
% --- -----------------------------------
if ~exist( [pwd '/bin'], 'dir' )
  mkdir bin
end;

% --- -----------------------------------
% --- delete all files from ./bin directory ( binaries )
% --- -----------------------------------
!rm -fr ./bin/*


if create_shell_files
  % --- -----------------------------------
  % --- determine MCRROOT for shell run_* creation
  % --- -----------------------------------
  s = which( 'svd' );
  n = strfind( s, [ filesep() 'toolbox' ] );
  MCRROOT = s(11:n);

  if ~exist( [pwd '/run'], 'dir' )
    mkdir run
  end
  delete ./run/*

end

  
% --- -----------------------------------
% --- Threading is on by default, but may need to be disabled on WestGrid
% --- -----------------------------------
singleThread = '';
%singleThread = '-R -singleCompThread ';  % --- uncomment to disable threading - may be required on Westgrid


% --- -----------------------------------
% --- Generic compiler options for clarity of compile command
% --- -----------------------------------
copts  = ['-C -R -nojvm -R -nodisplay ' singleThread '-m -v -w enable'];
common  = ' -I process_code/common';
filesys = ' -I process_code/file_system';


% --- -----------------------------------
% --- Z Creation/Normalization
% --- -----------------------------------

INC = '';
SCRIPT = 'iterationstep';

disp( '% --- ' );
disp( ['% --- Make ' SCRIPT '() ' ]);
eval( [ 'mcc ' copts common INC ' -d bin ' SCRIPT '.m' ] )

if create_shell_files
  eval( [ '!cat exec_scripts/local/run_script_template.txt | sed ''s!__MCR__!' MCRROOT '!'' | sed ''s!__BIN__!' SCRIPT '!'' > run/run_' SCRIPT '.sh' ] ) ;
end

% --- -----------------------------------
% --- delete extraneous files from ./bin directory ( binaries )
% --- -----------------------------------
delete ./bin/*.log
delete ./bin/*.sh
delete ./bin/readme.txt


if create_shell_files
  eval( [ '!chmod +x run/*.sh' ] );
end
