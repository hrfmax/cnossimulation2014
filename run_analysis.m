function [] = run_analysis(simulation_name)
% RUN_ANALYSIS starts analysis of the current data
% Performs evaluation of the resulting rotated matrices and tests for
% significant differences in performanc. 
%
% @author Konstantin Helmsauer, Jan Boelts
% {konstantinhelmsauer}{j.f.boelts}@gmail.com

mkdir(['results/' simulation_name]);

load(['results/' simulation_name]);
job_file = load(['job_files/' simulation_name]);

fid = fopen(['results/' simulation_name '/' simulation_name '_test.txt'], 'w');

evaluation.cor2.quartim = evaluation.cor2.quartimax;            

methods = {'unrot','random','varim','quartim','promax','hrfmax','plite', 'olite','procr','obl'};

fprintf(fid, ['EVALUATION' simulation_name '\n']);

for cur_method_1 = 1:length(methods)
    
    fprintf(fid, '\n\n');
    fprintf(fid, ['==========\t\tLOOK AT ' upper(methods{cur_method_1}) '\t\t\t==========\n']);

    for cur_method_2 = 1:length(methods)
        
        fprintf(fid, '\n');
        fprintf(fid, ['----------\tCompare ' methods{cur_method_1} ' with ' methods{cur_method_2} '\t----------\n\n']);

        for ni = 1:length(job_file.noise_perc)
           
            eval(['cur_v_1 = evaluation.cor2.' methods{cur_method_1} '(ni,:);']);
            eval(['cur_v_2 = evaluation.cor2.' methods{cur_method_2} '(ni,:);']);
            
            
            [p,~] = signrank(cur_v_1, cur_v_2);
            
            fprintf(fid, [num2str(job_file.noise_perc(ni)) ' Noise:\tp = ' num2str(p)]);
            
                  
            if p < .05
                fprintf(fid, '\t\t\t*');
            end
            
            if p < .01
                fprintf(fid, '*');
            end
            
            if p < .001
                fprintf(fid, '*');
            end
            
            fprintf(fid, '\n');
            
            fprintf(fid, ['\t\t\tMedian ' methods{cur_method_1} ': ' num2str(median(cur_v_1)) '\n']);
            fprintf(fid, ['\t\t\tMedian ' methods{cur_method_2} ': ' num2str(median(cur_v_2)) '\n']);


           
        end
        
    end
    
end

analysis_plot(simulation_name);

end