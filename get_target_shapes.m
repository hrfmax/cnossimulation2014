function [ target_shapes ] = get_target_shapes(num_subjects, num_timebins, num_conditions, TR, events)
% GET_TARGET_SHAPES generates a matrix of target shapes that will be used
% as target matrix for the procrustes rotation method. 
% This function generates a matrix containing a set of target columns that
% contain given hrf shapes. The columns represent the components generated
% by CPCA and subsets of them will be used as target matrix in the
% procrustes rotation that produces a P weight matrix that is as similar as
% possible to the target-matrix. 
% The target columns have a number of rows that corresponds to the number
% of subjects, timebins and conditions and will be the same as in p-model
% or the p matrix resulting from CPCA. 
% INPUT
%   num_subjects    number of subjects
%   num_timebins    number of timebins
%   num_conditions  number of conditions in the current iteration
%   TR              time repetition used by the fMRI scanner
%   events          cell array containing all possible events specified
%                   with onset and duration
%
% @author Konstantin Helmsauer, Jan Boelts
% {konstantinhelmsauer}{j.f.boelts}@gmail.com

% get number of possible shapes
num_shapes = length(events);

% Check for valid input arguments or set default values
if num_subjects<=0 || num_shapes < 1 || num_timebins < 5
    display('stupid input arguments in get_p_model!');
end

% Similar to when generating the p_model matrix we here use a SPM function 
% to generate of hrf shape for each guessed event (a pair of onset and
% duration) 
% the resulting shapes cell array contains the hrf shapes as single cells
% with as many entries as there are timebins
shapes = cellfun(@(x)calculate_hrf_shape(x, num_timebins, TR), events, 'UniformOutput', 0);
% vertically concatenate the shapes matrix. 
target_shapes = repmat(cell2mat(shapes), num_conditions*num_subjects,1);

% in case we have only one condition the shapes will be the same for each
% timebin-section in the components
% however, if there are more conditions, the timebin section within one
% component can differ because different conditions can have different
% shapes
if num_conditions > 1
    
    % first, add the theoretically possible zero shape
    shapes{num_shapes+1} = zeros(1, num_timebins);
    % increment numebr of shapes
    num_shapes = num_shapes + 1;
    
    % get all possible combinations to take #conditions shapes from the set
    % of possible shapes, irrespective of order
    C = nchoosek(1:num_shapes,num_conditions);
    % now take add the remaining possibilities (reverse the order) 
    % THIS WORKS ONLY IF THERE ARE NEVER MORE THAN TWO CONDITIONS!!!
    C = [C ; C(:,2) C(:,1)];
%     permu = perms(1:num_shapes);
%     C = permu(:,1:num_conditions);
%     C = unique(C, 'rows');

    target_shapes = [target_shapes zeros(size(target_shapes,1), size(C,1))]; 
    
    for i=1:size(C,1) % For each combination
        
        % prelocate the shape for one single subject 
        cur_shape = zeros(num_conditions*num_timebins, 1);
        
        % now fill the column with shapes 
        for cond_i=1:num_conditions 
            % for each condition take different shape 
            cur_shape(((cond_i-1)*num_timebins+1):(cond_i*num_timebins), :) = shapes{C(i, cond_i)};
            
        end
        % repeat the set of shapes for all subjects
        cur_shape = repmat(cur_shape, num_subjects, 1);
        % add this possible set of shape to the set of target shapes 
        target_shapes(:,(num_shapes-1)+i) = cur_shape;
        
    end
    
end

end
