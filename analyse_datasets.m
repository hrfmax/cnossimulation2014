% @author Konstantin Helmsauer, Jan Boelts
% {konstantinhelmsauer}{j.f.boelts}@gmail.com
% Manually put it the results you want to analyze
names={'Dec4_1cond2comp200it_varimax','Dec4_2cond2comp200it_varimax',...
    'Dec4_1cond4comp200it_varimax','Dec4_2cond4comp200it_varimax'};

for i=1:length(names)
    % call the analysis wrapper
   analysis_wrapper(names{i});
end