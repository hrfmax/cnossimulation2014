%% RAUSCHER 
% This script tries out different ways of adding noise to the theoretical
% model of the simulation
%
% @author Konstantin Helmsauer, Jan Boelts
% {konstantinhelmsauer}{j.f.boelts}@gmail.com

rng('shuffle')
num_conditions = 2;
num_components = 2;
num_timebins = 10;
num_subjects = 1000;
TR = 2;
num_repetitions = 2;
v_mode = 'random';
noise  = .5;

% load original V matrix from WM study
if strcmp(v_mode,'random')
    V=randn(15,num_components); % ------------------------------
else
    load(['V_matrices/V_' v_mode '_' num2str(num_components) 'c.mat']);
end

if size(V,2) ~= num_components
    error('Wrong V Matrix Column Number');
end

% how many scans per subject?
num_scans = num_conditions*num_timebins*num_repetitions;

seed = int32(randn());
events = choose_events(num_conditions, num_components,'shuffle');

% now create the p model using the new events
p_model = get_p_model(num_subjects, num_timebins, TR, events, 0);
% p_model = randn(size(p_model,1), size(p_model,2)); % ------------------------------


% guessed shapes contains all the induced events plus 2 distractors
tempevents = events';
if num_conditions==1
    guessed_events = [events{:}, {[0 2]}, {[4 2]}];
else
    guessed_events = [tempevents{((num_components/2)+1):end}, {[0 2]}, {[4 2]}];
end

num_guessed_events = length(guessed_events);

% Compute all combinations of putting all guessed events on every
% component and condition
target_shapes = get_target_shapes(num_subjects, num_timebins, num_conditions, TR, guessed_events);

% Create a dummy coded G Matrix (num_subjects * num_scans) by (num_subj * num_timebins * num_conditions)
G = createG(num_subjects, num_conditions, num_repetitions, num_timebins);

% Create U from G and P
V = randn(10,num_components);

U = G*p_model;

U = zscore(U); % mean-center U

M = U*V';

noise_coeff = noise/(1-noise);

max_std = max(std(M)) + max(std(M))*noise_coeff; % maximal standard deviation of a voxel

M_noisy = M;

for col = 1:size(M,2)
   
    cur_col = M(:,col);
    col_noise_std = sqrt(max_std^2 - (sum(cur_col.^2 - 2*cur_col + 2*mean(cur_col)))/length(cur_col));
    M_noisy(:,col) = cur_col + randn(size(cur_col,1), size(cur_col,2))*col_noise_std;
    
end

std(M_noisy)