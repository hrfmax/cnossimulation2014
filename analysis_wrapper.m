function []=analysis_wrapper(simulation_name)
% ANALYSIS_WRAPPER meta function for the analysis
% This function is the meta function for the analysis of the simulation
% results. It calls various functions for the statistical analysis and the
% visualization of the data. 
%
% @author Konstantin Helmsauer, Jan Boelts
% {konstantinhelmsauer}{j.f.boelts}@gmail.com

% Check whether dataset complete, e.g., check for all iterations
disp('Dataset Complete? ...');
job_file = load(['job_files/' simulation_name]);
for n = 1:length(job_file.noise_perc)
    for iter = 1:job_file.iterations
        if ~exist(['simulations/' simulation_name '/n' num2str(n) '_i' num2str(iter) '.mat'], 'file')
            disp(['simulations/' simulation_name '/n' num2str(n) '_i' num2str(iter) '.mat: does not exist']);
        end
    end
end

% make directories for results and call all needed functions
mkdir(['results/' simulation_name]);

disp('Make Evaluation File ...');
make_evaluation_file(simulation_name);

disp('Analyse P Matrices ...');
run_analysis_P(simulation_name);

disp('Analyse V Matrices ...');
run_analysis_V(simulation_name);

disp('Plot Results ...');
analysis_plot(simulation_name);

%disp('Make Report ...');
%make_report(simulation_name);

end