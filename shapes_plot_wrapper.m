function [] = shapes_plot_wrapper(simulation_name)
% SHAPES_PLOT_WRAPPER is the meta function for the plotting of results. It
% finds the best iteration of the current experiment to improve
% visualization
%
% @author Konstantin Helmsauer, Jan Boelts
% {konstantinhelmsauer}{j.f.boelts}@gmail.com
!rm shapes_n*

job_file = load(['job_files/' simulation_name '.mat']);
evaluation_file = load(['results/' simulation_name '.mat']);

methods = {'unrot', 'random', 'varim', 'hrfmax', 'procr', 'obl'};

for me = 1:length(methods)
    
    eval(['cur_cors_matrix = evaluation_file.evaluation.med_spear.' methods{me} ';']);
    
    for ni = 1:length(job_file.noise_perc)
        
        best_cor = 0;
        best_it = 0;
        
        % Find best iteration
        for it = 1:job_file.iterations
            cur_cor = cur_cors_matrix(ni, it);
            if cur_cor > best_cor
                best_it = it;
                best_cor = cur_cor;
            end
        end
        
        % Plot shapes for iteration
        best_file = load(['simulations/' simulation_name '/n' num2str(ni) '_i' num2str(best_it) '.mat']);
        eval(['best_file_p = best_file.p_' methods{me} ';']);
        shapes_plot(simulation_name, ni, best_it, best_file.p_model, best_file_p, job_file.num_timebins, job_file.num_conditions, job_file.num_components, methods{me});        
        close all
    end
    
end
