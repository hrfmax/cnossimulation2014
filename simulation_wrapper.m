function []=simulation_wrapper(simulation_name, num_noise_steps, num_iterations)
% This is a test script. Everything that is done here will be done by 
% a shell script on the cluster later on.
%
% @author Konstantin Helmsauer, Jan Boelts
% {konstantinhelmsauer}{j.f.boelts}@gmail.com

mkdir(['simulations/' simulation_name]);

for ni = 1:num_noise_steps
    
    for it = 1:num_iterations
        
        % qsub this line!
        iterationstep(simulation_name, ni, it);
        
    end
    
end

%---- Shell script ends here

make_evaluation_file(simulation_name);