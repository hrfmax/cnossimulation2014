Overview over code structure
============================

The core script of this simulation is iterationscript.m. If called, it simulates one fMRI-dataset and analyzes it using CPCA and various rotation methods. The ground truth parameters that specify the simulation (e.g. number of subjects, number of ground truth components, noise levels, total number of simulated datasets...) are loaded from a "job file" in the respective directory. A V matrix is randomly generated or loaded from the "V_matrices/" directory. "iterationstep.m" calls several functions to choose random events to be included in the dataset ("choose_events.m"), to create a G matrix for the simulated experiment ("createG.m"), to calculate a ground truth P matrix ("get_p_model"), to choose some suspected HRF shapes ("get_target_shapes.m") and for rotation ("hrfmax.m", "my_procrustes.m"). Simulation data is saved in the "simulations/" directory.

After simulation, the quality of the analysis methods can be assessed by calling "analysis_wrapper.m". It takes simulation results from "simulations/" and stores analysis files and plots in "results/". 


Running a compiled version of the simulation (on a cluster)
===========================================================

All other directories ("bin/", "exec_scripts/", "process_code/", "run/") are to run a compiled version of the code on a cluster. If the compiler and the runtime are properly installed this should work. The compiled version of an iterationstep can then be run (e.g. on a cluster) by calling (a possibly adapted version of) "run/run_iterationstep.sh". "submitjobs" is a shell script that might be helpful to see how one complete simulation, comprising many iterations and many noise levels, is then run on westgrid.