#!/bin/bash

if [ $# -lt "1" ]
then
  echo " syntax: ${0} process"
  exit 1
fi

subjectlist="1 2 3 4 5"

case $1 in

  # --------------------------------------------------
  # - normalize subect data
  # --------------------------------------------------
  'normalize')

    runthese=$subjectlist
    for s in ${runthese}
    do
      ./run/run_create_dipole_Z.sh $s
    done
  ;;


  # --------------------------------------------------
  # - accumulate Subject SSQ stats
  # --------------------------------------------------
  'sum_Z')
    ./run/run_accumulate_Z_SSQ.sh
  ;;


  # --------------------------------------------------
  # - Create G
  # --------------------------------------------------
  'create_G')
    runthese=$subjectlist
    for s in ${runthese}
    do

      # --------------------------------------------------
      # --- only create the GHeader file on the first subject
      # --- creation flags = [ Gheader, G, GG, gg ]
      # --------------------------------------------------
      if [ $s = "1" ]
      then
        flags="1111"
      else
        flags="0111"
      fi

      ./run/run_create_G.sh $s ${flags}
    done

  ;;




  # --------------------------------------------------
  # - Regress G
  # --------------------------------------------------
  'regress')

    runthese=$subjectlist
    for s in ${runthese}
    do

      # --------------------------------------------------
      # --- only add GZheader to the GHeader file on the first subject
      # --------------------------------------------------
      if [ $s = "1" ]
      then
        flags="1"
      else
        flags="0"
      fi

      ./run/run_regress_G.sh $s ${flags}
    done

  ;;



  # --------------------------------------------------
  # - accumulate GC SSQ stats
  # --------------------------------------------------
  'sum_GC')
    ./run/run_accumulate_GC_SSQ.sh
  ;;


  # --------------------------------------------------
  # - create full CC matrix and calculate Eigenvalues
  # --------------------------------------------------
  'CC')
    ./run/run_create_CC.sh
  ;;





  # --------------------------------------------------
  # - help 
  # --------------------------------------------------
   * )
    echo " syntax: ${0} process"
    echo "process: { normalize | sum_Z | create_G | regress | sum_GC | CC }"
    echo " "
#    echo "  * requires the number of components in parameter list "
#    echo "    normalization and G application do not require parameterization - they are coded into this script."
#    echo " "
  ;;

esac
