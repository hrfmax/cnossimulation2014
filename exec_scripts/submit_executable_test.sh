#!/bin/bash

if [ $# -lt "1" ]
then
  echo " syntax: ${0} process {process parameters}"

    echo " "
    echo "% ----------------------------"
    echo "% --- process: init "
    echo "% --- To initialize data structures: "
    echo "% ---   $0 init files.lst  mask.img "
    echo "% ---      use 'create' for any parameter, or actual existing file"

    echo " "
    echo "% ----------------------------"
    echo "% --- process: normalize "
    echo "% --- scan normalization: "
    echo "% ---   $0 normalize"
    echo "% ---      set parameters inside script"

    echo " "
    echo "% ----------------------------"
    echo "% --- process: create_g "
    echo "% --- G creation: "
    echo "% ---   $0 create_g"
    echo "% ---      no parameters required - loaded from pars.mat conditions "

    echo " "
    echo "% ----------------------------"
    echo "% --- process: apply "
    echo "% --- GZ creation: "
    echo "% ---   $0 apply"
    echo "% ---      set parameters inside script"

    echo " "
    echo "% ----------------------------"
    echo "% --- process: finalize "
    echo "% --- GC creation: "
    echo "% ---   $0 finalize"
    echo "% ---      no parameters"

    echo " "
    echo "% ----------------------------"
    echo "% --- process: extract "
    echo "% --- Component Extraction: "
    echo "% ---   $0 extract  {number_of_components}"

    echo " "
    echo "% ----------------------------"
    echo "% --- process: images "
    echo "% --- Extracted components images: "
    echo "% ---   $0 images  {number_of_components}"

    echo " "
    echo "% ----------------------------"
    echo "% --- process: rotate "
    echo "% --- Rotate components: "
    echo "% ---   $0 rotate  {number_of_components}"
    echo "% ---   note: scan_information.processing.{model}.rotation will need to be set"
    echo "% ---       : rotation structures for use located in rotation_definitions.mat"

  exit
fi


case "$1" in 
  'init')
    qsub -l walltime=00:01:00,mem=2gb -N init -v parms="$2 $3 $4" -V run_init_ZInfo.sh
   ;;

  'normalize')
    qsub -l walltime=00:01:00,mem=2gb -N NRM_1_1 -v parms="1 1 1111100" -V run_normalize.sh
    qsub -l walltime=00:01:00,mem=2gb -N NRM_2_2 -v parms="2 2 1111100" -V run_normalize.sh
    qsub -l walltime=00:01:00,mem=2gb -N NRM_3_3 -v parms="3 3 1111100" -V run_normalize.sh
# parms:
#    $1: normalize
#
#    1: subject number to start normalizing from
#    2: subject number to end normalizing at
#                   1             2            3              4       5            6              7
#    3: flags :  meanCenter  standardize  applyRegression  Linear  Quadratic  headMovement  summarizeTsums
#                   note: any regression selection will require flag(3) set to 1 
#
#  use the summarize flag on a last normalized subject after all previous normalizations have completed
   ;;

  'last_norm')
    qsub -l walltime=00:01:00,mem=2gb -N NRM_4 -v parms="4 4 1111101" -V run_normalize.sh
   ;;


  'create_g')
    qsub -l walltime=00:05:00,mem=8gb -N create_G -v parms="na" -V run_init_G.sh
   ;;

  'apply')
    qsub -l walltime=00:01:00,mem=2gb -N GZ_1_1 -v parms="1 1 10" -V run_apply_g.sh
    qsub -l walltime=00:01:00,mem=2gb -N GZ_2_2 -v parms="2 2 10" -V run_apply_g.sh
    qsub -l walltime=00:01:00,mem=2gb -N GZ_3_3 -v parms="3 3 10" -V run_apply_g.sh
    qsub -l walltime=00:01:00,mem=2gb -N GZ_4_4 -v parms="4 4 10" -V run_apply_g.sh
# parms:
#    $1: apply
#
#    1: subject number to start GZ Creation from
#    2: subject number to end GZ Creation at
#                   1             2       
#    3: flags :  create GZ    create E
#                   note: E creation not implemented in this version
   ;;

  'GCSD')
    qsub -l walltime=00:05:00,mem=8gb -N GCSD -v parms="1" -V run_GCSD.sh
   ;;

  'finalize')
    qsub -l walltime=01:30:00,mem=8gb -N GCC_S21 -v parms="na" -V run_finalize_GCC.sh
   ;;

  'extract')
    qsub -l walltime=18:00:00,mem=45gb -N G_Extract_${2} -v parms="${2}" -V run_extract_from_G.sh
   ;;

  'images')
    qsub -l walltime=00:05:00,mem=8gb -N G_Images_${2} -v parms="${2}" -V run_G_images.sh
   ;;

  'rotate')
    qsub -l walltime=00:05:00,mem=8gb -N rotate_${2} -v parms="$2" -V run_do_rotation.sh
   ;;

  'extract_ss')
    rm -f ss_cov_S*
    qsub -l walltime=00:05:00,mem=8gb -N ssc_1_1 -v parms="1 1 $2" -V run_unrotated_subject_components.sh
    qsub -l walltime=00:05:00,mem=8gb -N ssc_2_2 -v parms="2 2 $2" -V run_unrotated_subject_components.sh
    qsub -l walltime=00:05:00,mem=8gb -N ssc_3_3 -v parms="3 3 $2" -V run_unrotated_subject_components.sh
    qsub -l walltime=00:05:00,mem=8gb -N ssc_4_4 -v parms="4 4 $2" -V run_unrotated_subject_components.sh
  ;;

  'ss_summary')
    qsub -l walltime=00:05:00,mem=8gb -N ss_summary_${2} -v parms="$2" -V run_subject_components_summary.sh
   ;;


  'rotate_ss')
    rm ssr_cov_S*
#    qsub -l walltime=00:05:00,mem=8gb -N ssr_1_1 -v parms="1 1 $2 2" -V run_do_subject_rotation.sh  % --- separate for each rotation method
    qsub -l walltime=00:05:00,mem=8gb -N ssr_1_1 -v parms="1 1 $2 1" -V run_do_subject_rotation.sh
    qsub -l walltime=00:05:00,mem=8gb -N ssr_2_2 -v parms="2 2 $2 1" -V run_do_subject_rotation.sh
    qsub -l walltime=00:05:00,mem=8gb -N ssr_3_3 -v parms="3 3 $2 1" -V run_do_subject_rotation.sh
    qsub -l walltime=00:05:00,mem=8gb -N ssr_4_4 -v parms="4 4 $2 1" -V run_do_subject_rotation.sh
   ;;

  'r_ss_summary')
    qsub -l walltime=00:05:00,mem=8gb -N ssr_summary_${2} -v parms="$2" -V run_subject_components_rotated_summary.sh
   ;;


  'hrfmax')
    qsub -l walltime=00:05:00,mem=8gb -N hrfmax_${3} -v parms="$2 1 $3 $4 1 50" -V run_hrfmax.sh
# parms:
#    $1: hrfmax

#    $2: name of mat file containg P U and V  - no extension
#     1: perform oblique rotation
#    $3: number of iterations
#    $4: number of subjects
#     1: number of runs
#    50: length of progress indicator
   ;;


  'hselect')
    qsub -l walltime=00:05:00,mem=8gb -N hselect -v parms="$2 $3 500" -V run_prepare_H.sh
  ;;


  'GMH')
#   qsub -l walltime=00:15:00,mem=8gb -N GMH_GMH -v parms="GMH 111 5" -V run_GMH.sh
    qsub -l walltime=00:15:00,mem=8gb -N GMH_GC -v parms="GC 111 7" -V run_GMH.sh
  ;;

  '-h')
    echo " syntax: ${0} process { n_components}"
    echo "process: { normalize | apply | finalize | extract* | images* | extract_ss* | ss_summary* | rotate* | r_images* | rotate_ss* | r_ss_summary* }"
    echo " "
    echo "  * requires the number of components in parameter list "
    echo "    normalization and G application do not require parameterization - they are coded into this script."
    echo " "
  ;;

esac

user="jwhitman"
qstat  -f -n -1 -t -u jwhitman

