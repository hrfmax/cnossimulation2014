function [Rc] = congrcoeff(M)
% CONGROEFF Calculates Coefficient of Congruency between all columns in M
% This function calculates the coefficient of congruency as defined by
% Hartman 1976
%
% @author Konstantin Helmsauer, Jan Boelts
% {konstantinhelmsauer}{j.f.boelts}@gmail.com

% vectorized version
Rc = (M'*M)./(sqrt(sum(M.^2,1)'*sum(M.^2,1)));

end




