function [] = make_report(simulation_name)
%MAKE_REPORT prints the results of a simulation (stored in
% an evaluation file in ./results/ to a text file. Also
% calculates some minor summary statistics. Then 
% runs TeX to automatically generate a pdf-Report.
%
% INPUT
%	simulation_name		string specifying a simulation and
%						corresponding files in ./results/, 
%						./job_files/ and 
%
% @author Konstantin Helmsauer, Jan Boelts
% {konstantinhelmsauer}{j.f.boelts}@gmail.com

warning off

load(['results/' simulation_name]);
job_file = load(['job_files/' simulation_name]);

fid = fopen(['results/' simulation_name '/report_data.txt'], 'w');

methods = {'unrot','random','varim','quartimax','promax','hrfmax','plite', 'olite','procr','obl'};

fprintf(fid, ['\\newcommand{\\simulationname}{' simulation_name '}\n']);
fprintf(fid, ['\\newcommand{\\conditions}{' num2str(job_file.num_conditions) ' }\n']);
fprintf(fid, ['\\newcommand{\\components}{' num2str(job_file.num_components) ' }\n']);
fprintf(fid, ['\\newcommand{\\iterations}{' num2str(job_file.iterations) ' }\n']);
fprintf(fid, ['\\newcommand{\\vmode}{' job_file.v_mode ' }\n']);

% Medians
for cur_method = 1:length(methods)
    fprintf(fid, '\n\n');
     for ni = 1:length(job_file.noise_perc)
        eval(['cur_vector_p = evaluation.med_spear.' methods{cur_method} '(ni,:);']);
        eval(['cur_vector_v = evaluation.med_cc.' methods{cur_method} '(ni,:);']);
        fprintf(fid, ['\\newcommand{\\P' methods{cur_method} ''  num2word(ni) 'median}{' num2str(median(cur_vector_p)) '}\n']);
        fprintf(fid, ['\\newcommand{\\V' methods{cur_method} ''  num2word(ni) 'median}{' num2str(median(cur_vector_v)) '}\n']);
 
     end 
end

% Tests V Matrices
for cur_method_1 = 1:length(methods)
    fprintf(fid, '\n\n');
    for cur_method_2 = 1:length(methods)
        for ni = 1:length(job_file.noise_perc)
            eval(['cur_v_1 = evaluation.med_cc.' methods{cur_method_1} '(ni,:);']);
            eval(['cur_v_2 = evaluation.med_cc.' methods{cur_method_2} '(ni,:);']);            
            [p,~] = signrank(cur_v_1, cur_v_2);
            fprintf(fid, ['\\newcommand{\\V' methods{cur_method_1} '' methods{cur_method_2} '' num2word(ni) '}{' num2str(p) '}\n']);
        end
    end
end

% Tests P Matrices
for cur_method_1 = 1:length(methods)
    fprintf(fid, '\n\n');
    for cur_method_2 = 1:length(methods)
        for ni = 1:length(job_file.noise_perc)
            eval(['cur_v_1 = evaluation.med_spear.' methods{cur_method_1} '(ni,:);']);
            eval(['cur_v_2 = evaluation.med_spear.' methods{cur_method_2} '(ni,:);']);
            [p,~] = signrank(cur_v_1, cur_v_2);
            fprintf(fid, ['\\newcommand{\\P' methods{cur_method_1} '' methods{cur_method_2} '' num2word(ni) '}{' num2str(p) '}\n']);
        end
    end
end



copyfile('report_template.tex', ['results/' simulation_name '/' simulation_name '_report.tex']);

eval(['cd ' 'results/' simulation_name ])
eval(['! /usr/texbin/pdflatex ' simulation_name '_report.tex'])

end