function [output]=stats(p, q, method)

if nargin < 3
    error('specify statistics!');
end

if ((size(p,2) ~= size(q,2)) || (size(p,1) ~= size(q,1)))
    error('size of input matrices p and q are different')
end

switch method
    case {'pearson', 'spearman', 'congrcoeff'}
        output = 0;
    case {'sum_of_squares'}
        output = Inf;
end

col_perms = perms(1:size(q,2));

cur_p = reshape(p, [], 1); % h�nge alle spalten in p untereinander

for i = 1:length(col_perms)
    
    q_col_perm = q(:,col_perms(i,:)); % Ordne Spalten in q um
    
    for comp_i  = 1:size(q, 2)
       
        if corr(p(:,comp_i), q_col_perm(:,comp_i)) < 0
            q_col_perm(:, comp_i) = -q_col_perm(:, comp_i);
        end
        
    end
    
%     cur_q = reshape(q_col_perm,[],1); % und h�nge sie untereinander 
    
    switch method
        
        case {'pearson', 'spearman'}
            
            % cur = abs(corr(cur_p, cur_q, 'type', method));
            
            cur = abs(corr(p, q_col_perm, 'type', method));
            cur = median(diag(cur));
            
            if cur > output
                output = cur;
            end
            
        case 'congrcoeff'
            cur = abs(totalcongrcoef(p, q_col_perm));
            
            if cur > output
                output = cur;
            end
            
        case 'sum_of_squares'
            cur = trace((p-q_col_perm)'*(p-q_col_perm));
            
            if cur < output
                output = cur;
            end
            
    end
            
    
end


