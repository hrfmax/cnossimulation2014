function [G] = createG(num_subjects, num_conditions, num_repetitions, num_timebins)

% Partial G Matrix for only one Subject and one Condition
SubjectG_OneCondition = repmat(eye(num_timebins, num_timebins),num_repetitions,1);

SubjectG_OneCondition_Rows = size(SubjectG_OneCondition, 1);
SubjectG_OneCondition_Cols = size(SubjectG_OneCondition, 2);

% Partial G Matrix for only one Subject but all Conditions
SubjectG = zeros(num_conditions*SubjectG_OneCondition_Rows,num_conditions*SubjectG_OneCondition_Cols);

for cond = 1:num_conditions
    row = (cond*SubjectG_OneCondition_Rows-(SubjectG_OneCondition_Rows-1)):(cond*SubjectG_OneCondition_Rows);
    col = (cond*SubjectG_OneCondition_Cols-(SubjectG_OneCondition_Cols-1)):cond*SubjectG_OneCondition_Cols;
    SubjectG(row,col)=SubjectG_OneCondition;
end

SubjectG_Rows = size(SubjectG, 1);
SubjectG_Cols = size(SubjectG, 2);

% Create G Matrix for all Subjects
G = zeros(num_subjects*SubjectG_Rows,num_subjects*SubjectG_Cols);

for subj = 1:num_subjects
    row = (subj*SubjectG_Rows-(SubjectG_Rows-1)):(subj*SubjectG_Rows);
    col = (subj*SubjectG_Cols-(SubjectG_Cols-1)):subj*SubjectG_Cols;
    G(row,col)=SubjectG;
end

end

