function [ tcc ] = totalcongrcoef(A,B)

tcc = trace(A'*B) / (sqrt(trace(A'*A))*sqrt(trace(B'*B)));

end

