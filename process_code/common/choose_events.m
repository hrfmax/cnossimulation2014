function [eventset]=choose_events(num_conditions, num_components, seed)
% CHOOSE_EVENTS generates a set of hrf events
% This function creates a cell array of hrf events that will be included in
% the simulated fMRI data. Depending on the number of conditions and the
% number of components in the current iteration, a set of events is created
% by stating the onset and the duration of the event. Both are saved in a
% cell array that is used for the further analysis. 
% INPUT 
%   num_conditions      number of conditions used in the current iteration
%   num_components      number of components in the current iteration
%   seed                seed for the random number generator (RNG)
% OUTPUT 
%   eventset            cell array with events for all combinations of
%                       conditions and components specified by onset and 
%                       duration
% @author: Jan and Konstantin (j.f.boelts@gmail.com)

% check number of components
if mod(num_components,2) ~= 0
   error('only even number of component allowed, bitch!'); 
end

% set seed for RNG
rng(seed);

% prelocate the eventset cell array
eventset = cell(num_conditions, num_components);

% set the onsets and durations
onsets = 0:0.2:8;
durations = 0:0.2:4;
% take Kronecker Tensor Product (kron) to copy each onset in column vector
% with the same length as durations
abc(:,1) = kron(onsets, ones(1,length(durations)));
% concatenate the duration vector vertically onsets times 
abc(:,2) = repmat(durations', length(onsets),1);

% abc contains all combinations of onsets and durations, now shuffle them
abc = abc(randperm(size(abc,1)),:);
% a = abc(1:(num_conditions*num_components),:);
 
% copy all combinations of onsets and durations as single events into the
% eventset cell array 
for i = 1:(num_conditions*num_components)
    eventset{i} = abc(i,:);
end
 
% First half of components will have same event shape in both conditions.
% Condtions are rows in the eventset array; the first half of the the
% columns will contain the same numbers, respectively
if num_conditions>1
    for i=1:(num_components/2)
        eventset{1,i} = eventset{2,i};
    end
end