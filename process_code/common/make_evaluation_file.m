function [evaluation] = make_evaluation_file(simulation_name)

parameters = load(['./job_files/' simulation_name]);

iterations = parameters.iterations;
noise_steps = length(parameters.noise_perc);

V_model = load(['V_matrices/V_' parameters.v_mode '_' num2str(parameters.num_components) 'c.mat']);

%% Prelocate Variables
% use structures to prevent the variable name memory from exploding:
% structure for all sum of squares
evaluation.all_ss.unrot  = zeros(noise_steps,iterations);
evaluation.all_ss.procr  = zeros(noise_steps,iterations);
evaluation.all_ss.varim  = zeros(noise_steps,iterations);
evaluation.all_ss.hrfmax = zeros(noise_steps,iterations);
evaluation.all_ss.obl    = zeros(noise_steps,iterations);
evaluation.all_ss.random = zeros(noise_steps,iterations);
evaluation.all_ss.plite  = zeros(noise_steps,iterations);
evaluation.all_ss.olite  = zeros(noise_steps,iterations);

% structure for all standard correlations
evaluation.cor1.unrot     = zeros(noise_steps,iterations);
evaluation.cor1.varim     = zeros(noise_steps,iterations);
evaluation.cor1.hrfmax    = zeros(noise_steps,iterations);
evaluation.cor1.procr     = zeros(noise_steps,iterations);
evaluation.cor1.obl       = zeros(noise_steps,iterations);
evaluation.cor1.quartimax = zeros(noise_steps,iterations);
evaluation.cor1.promax    = zeros(noise_steps,iterations);
evaluation.cor1.random    = zeros(noise_steps,iterations);
evaluation.cor1.plite     = zeros(noise_steps,iterations);
evaluation.cor1.olite     = zeros(noise_steps,iterations);

% structure for all advanced correlations
evaluation.cor2.unrot     = zeros(noise_steps,iterations);
evaluation.cor2.varim     = zeros(noise_steps,iterations);
evaluation.cor2.hrfmax    = zeros(noise_steps,iterations);
evaluation.cor2.procr     = zeros(noise_steps,iterations);
evaluation.cor2.obl       = zeros(noise_steps,iterations);
evaluation.cor2.quartimax = zeros(noise_steps,iterations);
evaluation.cor2.promax    = zeros(noise_steps,iterations);
evaluation.cor2.random    = zeros(noise_steps,iterations);
evaluation.cor2.plite     = zeros(noise_steps,iterations);
evaluation.cor2.olite     = zeros(noise_steps,iterations);

% structure for all correlations of origibal V and rotated V
evaluation.corV.unrot     = zeros(noise_steps,iterations);
evaluation.corV.varim     = zeros(noise_steps,iterations);
evaluation.corV.quartimax = zeros(noise_steps,iterations);
evaluation.corV.promax    = zeros(noise_steps,iterations);
evaluation.corV.hrfmax    = zeros(noise_steps,iterations);
evaluation.corV.procr     = zeros(noise_steps,iterations);
evaluation.corV.obl       = zeros(noise_steps,iterations);
evaluation.corV.random    = zeros(noise_steps,iterations);
evaluation.corV.plite     = zeros(noise_steps,iterations);
evaluation.corV.olite     = zeros(noise_steps,iterations);

for ni=1:noise_steps
    
    for it = 1:iterations
        
        cur_it = load(['simulations/' simulation_name '/n' num2str(parameters.noise_perc(ni)) '_i' num2str(it) '.mat']);
        
        %% Evaluate Goodness Of Fit
                
        % Evaluate Sum of Squares
        evaluation.all_ss.unrot(ni, it)     = stats(cur_it.p_unrot, cur_it.p_model,'sum_of_squares');
        evaluation.all_ss.procr(ni, it)     = stats(cur_it.p_procr, cur_it.p_model,'sum_of_squares');
        evaluation.all_ss.varim(ni, it)     = stats(cur_it.p_varim, cur_it.p_model,'sum_of_squares');
        evaluation.all_ss.quartimax(ni, it) = stats(cur_it.p_quartimax, cur_it.p_model,'sum_of_squares');
        evaluation.all_ss.promax(ni, it)    = stats(cur_it.p_promax, cur_it.p_model,'sum_of_squares');
        evaluation.all_ss.hrfmax(ni, it)    = stats(cur_it.p_hrfmax, cur_it.p_model,'sum_of_squares');
        evaluation.all_ss.obl(ni, it)       = stats(cur_it.p_obl, cur_it.p_model,'sum_of_squares');
        evaluation.all_ss.random(ni, it)    = stats(cur_it.p_random, cur_it.p_model,'sum_of_squares');
        evaluation.all_ss.plite(ni, it)     = stats(cur_it.p_plite, cur_it.p_model,'sum_of_squares');
        evaluation.all_ss.olite(ni, it)     = stats(cur_it.p_olite, cur_it.p_model,'sum_of_squares');
       
        % standard evaluation
        evaluation.cor1.unrot(ni,it)    = stats(cur_it.p_unrot, cur_it.p_model,'pearson');
        evaluation.cor1.varim(ni,it)    = stats(cur_it.p_varim, cur_it.p_model,'pearson');
        evaluation.cor1.quartimax(ni,it)= stats(cur_it.p_quartimax, cur_it.p_model,'pearson');
        evaluation.cor1.promax(ni,it)   = stats(cur_it.p_promax, cur_it.p_model,'pearson');
        evaluation.cor1.hrfmax(ni,it)   = stats(cur_it.p_hrfmax, cur_it.p_model,'pearson');
        evaluation.cor1.procr(ni,it)    = stats(cur_it.p_procr, cur_it.p_model,'pearson');
        evaluation.cor1.obl(ni,it)      = stats(cur_it.p_obl, cur_it.p_model,'pearson');
        evaluation.cor1.random(ni,it)   = stats(cur_it.p_random, cur_it.p_model,'pearson');
        evaluation.cor1.plite(ni,it)   = stats(cur_it.p_plite, cur_it.p_model,'pearson');
        evaluation.cor1.olite(ni,it)   = stats(cur_it.p_olite, cur_it.p_model,'pearson');
      
        % Our Evaluation
        evaluation.cor2.unrot(ni,it)     = stats(cur_it.p_unrot, cur_it.p_model,'spearman');
        evaluation.cor2.varim(ni,it)     = stats(cur_it.p_varim, cur_it.p_model,'spearman');
        evaluation.cor2.promax(ni,it)    = stats(cur_it.p_promax, cur_it.p_model,'spearman');
        evaluation.cor2.quartimax(ni,it) = stats(cur_it.p_quartimax, cur_it.p_model,'spearman');
        evaluation.cor2.hrfmax(ni,it)    = stats(cur_it.p_hrfmax, cur_it.p_model,'spearman');
        evaluation.cor2.procr(ni,it)     = stats(cur_it.p_procr, cur_it.p_model,'spearman');
        evaluation.cor2.obl(ni,it)       = stats(cur_it.p_obl, cur_it.p_model,'spearman');
        evaluation.cor2.random(ni,it)    = stats(cur_it.p_random, cur_it.p_model,'spearman');
        evaluation.cor2.plite(ni,it)     = stats(cur_it.p_plite, cur_it.p_model,'spearman');
        evaluation.cor2.olite(ni,it)     = stats(cur_it.p_olite, cur_it.p_model,'spearman');
        
        % Quantify similarity between original V matrix and rotated V
        % matrix
        
        evaluation.corV.unrot(ni,it)     = stats(cur_it.V_unrot, V_model.V,'congrcoeff');
        evaluation.corV.varim(ni,it)     = stats(cur_it.V_varim, V_model.V,'congrcoeff');
        evaluation.corV.quartimax(ni,it) = stats(cur_it.V_quartimax, V_model.V,'congrcoeff');
        evaluation.corV.promax(ni,it)    = stats(cur_it.V_promax, V_model.V,'congrcoeff');
        evaluation.corV.hrfmax(ni,it)    = stats(cur_it.V_hrfmax, V_model.V,'congrcoeff');
        evaluation.corV.procr(ni,it)     = stats(cur_it.V_procr, V_model.V,'congrcoeff');
        evaluation.corV.obl(ni,it)       = stats(cur_it.V_obl, V_model.V,'congrcoeff');
        evaluation.corV.plite(ni,it)     = stats(cur_it.V_plite, V_model.V,'congrcoeff');
        evaluation.corV.olite(ni,it)     = stats(cur_it.V_olite, V_model.V,'congrcoeff');
        evaluation.corV.random(ni,it)    = stats(cur_it.V_random, V_model.V,'congrcoeff');
                
        
    end
    
end


save(['results/' simulation_name '.mat'], 'evaluation');

end