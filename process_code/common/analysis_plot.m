% aNALYSE dATA eTC.

function []=analysis_plot(simulation_name)

close all
set(0, 'DefaulttextInterpreter', 'none')

load(['results/' simulation_name])
parameters  = load(['job_files/' simulation_name]);


%% Lower and Upper Quartiles (noise-level dependent)
quantiles = [.25 .5 .75];
quart_corV_unrot = quantile(evaluation.corV.unrot, quantiles, 2);
quart_corV_varim = quantile(evaluation.corV.varim, quantiles, 2);
quart_corV_quartimax = quantile(evaluation.corV.quartimax, quantiles, 2);
quart_corV_promax = quantile(evaluation.corV.promax, quantiles, 2);
quart_corV_quartimax = quantile(evaluation.corV.quartimax, quantiles, 2);
quart_corV_hrfmax = quantile(evaluation.corV.hrfmax, quantiles, 2);
quart_corV_procr = quantile(evaluation.corV.procr, quantiles, 2);
quart_corV_obl = quantile(evaluation.corV.obl, quantiles, 2);
quart_corV_olite = quantile(evaluation.corV.olite, quantiles, 2);
quart_corV_plite = quantile(evaluation.corV.plite,quantiles, 2);
quart_corV_random = quantile(evaluation.corV.random, quantiles, 2);

quart_cor2_unrot = quantile(evaluation.cor2.unrot, quantiles, 2);
quart_cor2_varim = quantile(evaluation.cor2.varim, quantiles, 2);
quart_cor2_quartimax = quantile(evaluation.cor2.quartimax, quantiles, 2);
quart_cor2_promax = quantile(evaluation.cor2.promax, quantiles, 2);
quart_cor2_hrfmax = quantile(evaluation.cor2.hrfmax, quantiles, 2);
quart_cor2_procr = quantile(evaluation.cor2.procr, quantiles, 2);
quart_cor2_obl = quantile(evaluation.cor2.obl, quantiles, 2);
quart_cor2_olite = quantile(evaluation.cor2.olite, quantiles, 2);
quart_cor2_plite = quantile(evaluation.cor2.plite, quantiles, 2);
quart_cor2_random = quantile(evaluation.cor2.random, quantiles, 2);

median_cor2_unrot = median(evaluation.cor2.unrot, 2);
median_cor2_random = median(evaluation.cor2.random, 2);
median_cor2_varim = median(evaluation.cor2.varim, 2);
median_cor2_quartimax = median(evaluation.cor2.quartimax, 2);
median_cor2_promax = median(evaluation.cor2.promax, 2);
median_cor2_hrfmax = median(evaluation.cor2.hrfmax, 2);
median_cor2_plite = median(evaluation.cor2.plite, 2);
median_cor2_olite = median(evaluation.cor2.olite, 2);
median_cor2_procr = median(evaluation.cor2.procr, 2);
median_cor2_obl = median(evaluation.cor2.obl, 2);

median_corV_unrot = median(evaluation.corV.unrot, 2);
median_corV_random = median(evaluation.corV.random, 2);
median_corV_varim = median(evaluation.corV.varim, 2);
median_corV_quartimax = median(evaluation.corV.quartimax, 2);
median_corV_promax = median(evaluation.corV.promax, 2);
median_corV_hrfmax = median(evaluation.corV.hrfmax, 2);
median_corV_plite = median(evaluation.corV.plite, 2);
median_corV_olite = median(evaluation.corV.olite, 2);
median_corV_procr = median(evaluation.corV.procr, 2);
median_corV_obl = median(evaluation.corV.obl, 2);


%% Noise dependent Correlation Plot
noise_cor2_median = figure;
hold all
plot(parameters.noise_perc, median_cor2_unrot, ':')
plot(parameters.noise_perc, median_cor2_random, '-.')
plot(parameters.noise_perc, median_cor2_varim, '--')
plot(parameters.noise_perc, median_cor2_promax, '--')
plot(parameters.noise_perc, median_cor2_hrfmax)
plot(parameters.noise_perc, median_cor2_plite)
plot(parameters.noise_perc, median_cor2_procr)
plot(parameters.noise_perc, median_cor2_obl)
legend('Unrotated', 'Random', 'Varimax',  'Promax', 'HRFMAX', 'Plite', 'Procr', 'Obl');
xlabel('Noise Percentage Added')
ylabel('Spearman Correlation Median Original vs. Extracted P')
title(['Noise dependent correlation plot' char(10) simulation_name])
saveas(noise_cor2_median,['results/' simulation_name '/' simulation_name '_P_sp_med.fig'])

%% Noise dependent Errorbar
noise_cor2_errorbar = figure;
hold all
errorbar(parameters.noise_perc, quart_cor2_unrot(:,2), quart_cor2_unrot(:,2)-quart_cor2_unrot(:,1),  quart_cor2_unrot(:,3)-quart_cor2_unrot(:,2))
errorbar(parameters.noise_perc, quart_cor2_random(:,2), quart_cor2_random(:,2)-quart_cor2_random(:,1),  quart_cor2_random(:,3)-quart_cor2_random(:,2))
errorbar(parameters.noise_perc, quart_cor2_varim(:,2), quart_cor2_varim(:,2)-quart_cor2_varim(:,1),  quart_cor2_varim(:,3)-quart_cor2_varim(:,2))
errorbar(parameters.noise_perc, quart_cor2_promax(:,2), quart_cor2_promax(:,2)-quart_cor2_promax(:,1),  quart_cor2_promax(:,3)-quart_cor2_promax(:,2))
errorbar(parameters.noise_perc, quart_cor2_hrfmax(:,2), quart_cor2_hrfmax(:,2)-quart_cor2_hrfmax(:,1),  quart_cor2_hrfmax(:,3)-quart_cor2_hrfmax(:,2))
errorbar(parameters.noise_perc, quart_cor2_plite(:,2), quart_cor2_plite(:,2)-quart_cor2_plite(:,1),  quart_cor2_plite(:,3)-quart_cor2_plite(:,2))
errorbar(parameters.noise_perc, quart_cor2_procr(:,2), quart_cor2_procr(:,2)-quart_cor2_procr(:,1),  quart_cor2_procr(:,3)-quart_cor2_procr(:,2))
errorbar(parameters.noise_perc, quart_cor2_obl(:,2), quart_cor2_obl(:,2)-quart_cor2_obl(:,1),  quart_cor2_obl(:,3)-quart_cor2_obl(:,2))
legend('Unrotated', 'Random', 'Varimax', 'Promax', 'HRFMAX', 'Plite', 'Procr', 'Obl');
xlabel('Noise Percentage Added')
ylabel('Spearman Correlation Original vs. Extracted P')
title(['Noise dependent correlation plot' char(10) simulation_name])
saveas(noise_cor2_errorbar,['results/' simulation_name '/' simulation_name '_P_sp_err.fig'])


%% Noise dependent Sum of Squares Plot
% noise_SS = figure;
% hold all
% plot(parameters.noise_perc, evaluation.means.SS.unrot)
% plot(parameters.noise_perc, evaluation.means.SS.random)
% plot(parameters.noise_perc, evaluation.means.SS.varim, '--')
% plot(parameters.noise_perc, evaluation.means.SS.quartimax, '--')
% plot(parameters.noise_perc, evaluation.means.SS.promax, '--')
% plot(parameters.noise_perc, evaluation.means.SS.hrfmax)
% plot(parameters.noise_perc, evaluation.means.SS.plite)
% plot(parameters.noise_perc, evaluation.means.SS.olite)
% plot(parameters.noise_perc, evaluation.means.SS.procr)
% plot(parameters.noise_perc, evaluation.means.SS.obl)
% legend('Unrotated', 'Random', 'Varimax', 'Quartimax', 'Promax', 'HRFMAX', 'Plite', 'Olite', 'Procr', 'Obl');
% xlabel('Noise Percentage Added')
% ylabel('Sum of Squares Original vs. Extracted P')
% title(['Noise dependent sum of squares plot' char(10) simulation_name])

%% Noise dependent Correlation Plot
noise_congrV = figure;
hold all
plot(parameters.noise_perc, median_corV_unrot, ':')
plot(parameters.noise_perc, median_corV_random, '-.')
plot(parameters.noise_perc, median_corV_varim, '--')
plot(parameters.noise_perc, median_corV_promax, '--')
plot(parameters.noise_perc, median_corV_hrfmax)
plot(parameters.noise_perc, median_corV_plite)
plot(parameters.noise_perc, median_corV_procr)
plot(parameters.noise_perc, median_corV_obl)
legend('Unrotated', 'Random', 'Varimax',  'Promax', 'HRFMAX', 'Plite', 'Procr', 'Obl');
xlabel('Noise Percentage Added')
ylabel('TCC Median Original vs. Extracted P')
title(['Noise dependent TCC plot' char(10) simulation_name])
saveas(noise_congrV,['results/' simulation_name '/' simulation_name '_V_tcc_med.fig'])

end

