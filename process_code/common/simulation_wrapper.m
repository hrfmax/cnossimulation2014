function []=simulation_wrapper(simulation_name, num_noise_steps, num_iterations)
% Testskript -- should be shell skript later on

mkdir(['simulations/' simulation_name]);

for ni = 1:num_noise_steps
    
    for it = 1:num_iterations
        
        % qsub this line!
        iterationstep(simulation_name, ni, it);
        
    end
    
end

%---- Shell script ends here

make_evaluation_file(simulation_name);