function [best_procr_p best_procr_t best_combo_procr best_obl_p best_obl_t best_combo_obl] = my_procrustes(p_unrot, shapes, combos)

num_components = size(p_unrot,2);

best_procr_p = p_unrot;
best_procr_t = eye(num_components);
best_cor_orth = 0;
best_ss_obl = Inf;
timing_procr = 0;
timing_obl = 0;

for i = 1:size(combos,1)
    
    p_model = shapes(:,combos(i,:));
    tic;
    %% Orthogonal Procrustes Rotation
    [L, ~, M] = svd(p_model' * p_unrot);
    t_procr = M * L';
    p_procr = p_unrot * t_procr;
    
    %% Goodness of Fit -- Orthogonal Rotation
    
    y = abs(corrcoef([p_procr p_model]));
    y = y((num_components+1):end, 1:num_components);
    p_procr_volume_cor = prod(max(y, [], 1));
    
    if p_procr_volume_cor > best_cor_orth
        best_procr_p = p_procr;
        best_procr_t = t_procr;
        best_cor_orth = p_procr_volume_cor;
        best_combo_procr = combos(i,:);
    end
    timing_procr = timing_procr + toc;
    
    %% Oblique Procrustes Rotation
    tic;
    if (rank(p_model) == num_components)
        t_obl = p_unrot \ p_model;
        t_obl = t_obl * diag(sqrt(diag((t_obl'*t_obl)\eye(size(p_model,2)))));
        p_obl = p_unrot * t_obl;
        
        % Ensure that rotated p contains only real numbers
        if isreal(p_obl)
            
            %% Goodness of Fit -- Oblique Rotation
            
            sum_squares_obl = sum(sum((p_model-p_obl).^2));
            
            if sum_squares_obl < best_ss_obl
                best_obl_p = p_obl;
                best_obl_t = t_obl;
                best_ss_obl = sum_squares_obl;
                best_combo_obl = combos(i,:);
            end
            
        end
        
    end
    timing_obl = timing_obl + toc;
end

