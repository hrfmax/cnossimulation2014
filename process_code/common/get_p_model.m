function [ p_model ] = get_p_model(num_subj, num_timebins, TR, events, subjectnoise)
% GET_P_MODEL Creates the underlying model-P-matrix
% This function creates the model-p-matrix that will be used to create the
% simulated data and that will be discovered by the rotation methods.
% Depending on the number of subjects, timebins, the TR and the event
% onsets and durations a p-matrix is constructed with components as
% columns. The components will contain various hrf shapes in different
% conditions. 
% INPUT
%   num_subj        number of subjects
%   nume_timebins   number of timebins that are modeled
%   TR              the time repetitions used for the fMRI 
%   events          cell array of all possible events specified with onset 
%                   and duration 
%   subjectnoise    percentage of individual subject noise in the shapes. 
% OUTPUT 
%   p_model         matrix with components as columns, containing the
%                   model-components used to create the data
% @author: Jan and Konstantin (j.f.boelts@gmail.com)

% get number of components and number of conditions from event cell array
num_components = size(events,2); 
num_conditions = size(events,1);

% Check for valid input arguments or set default values
if num_subj<=0 || num_components < 1 || num_timebins < 5
    display('stupid input arguments in get_p_model!');
end

%% Build p_model

% prelocate p_model
p_model=zeros(num_subj*num_timebins*num_conditions, num_components);

for subj_i = 1:num_subj % for each subject
    
    for comp_i = 1:num_components % for each component
        
        for cond_i = 1:num_conditions % for each condition 
            
            % if there is a NaN in the event matric
            if isnan(events{cond_i, comp_i}) 
                % just use a zero shape
                shape_to_insert  = zeros(1,num_timebins);
            % if not, generate a shape 
            else                
                % generate the possibly noisy event values: onset and
                % duration
                subj_cond_onsets = abs(events{cond_i, comp_i} + subjectnoise.*randn(size(events{cond_i, comp_i},1), size(events{cond_i, comp_i}, 2)));
                % generate the shape using the onset and the duration for
                % the current subject. This function is part of SPM
                shape_to_insert = calculate_hrf_shape(subj_cond_onsets, num_timebins, TR);                
            end
            
            % determine the rows in p-model that will be used
            rows = (((subj_i-1)*num_conditions*num_timebins+(cond_i-1)*num_timebins+1):((subj_i-1)*num_conditions*num_timebins+cond_i*num_timebins));
            % insert the hrf shape in current rows and the current
            % column(component) 
            p_model(rows, comp_i) = shape_to_insert;
            
        end
    end
end