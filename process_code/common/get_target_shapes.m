function [ target_shapes ] = get_target_shapes(num_subjects, num_timebins, num_conditions, TR, events)
% GET_TARGET_SHAPES generates a matrix of target shapes that will be used
% as target matrix for the procrustes rotation method. 
% This function generates a matrix containing a set of target columns that
% contain given hrf shapes. The columns represent the components generated
% by CPCA and subsets of them will be used as target matrix in the
% procrustes rotation that produces a P weight matrix that is as similar as
% possible to the target-matrix. 
% The target columns have a number of rows that corresponds to the number
% of subjects, timebins and conditions and will be the same as in p-model
% or the p matrix resulting from CPCA. 
% INPUT
%   num_subjects    number of subjects
%   num_timebins    number of timebins
%   num_conditions  number of conditions in the current iteration
%   TR              time repetition used by the fMRI scanner
%   events          cell array containing all possible events specified
%                   with onset and duration

% get number of possible shapes
num_shapes = length(events);

% Check for valid input arguments or set default values
if num_subjects<=0 || num_shapes < 1 || num_timebins < 5
    display('stupid input arguments in get_p_model!');
end

% use the SPM function to generate of hrf shape for each onset-duration
% pair 
% the resulting shapes cell array contains the hrf shapes as column vectors
shapes = cellfun(@(x)calculate_hrf_shape(x, num_timebins, TR), events, 'UniformOutput', 0);
% vertically concatenate the shapes matrix. 
target_shapes = repmat(cell2mat(shapes), num_conditions*num_subjects,1);

if num_conditions > 1
    
    shapes{num_shapes+1} = zeros(1, num_timebins);
    num_shapes = num_shapes + 1;
    
    
    C = nchoosek(1:num_shapes,num_conditions);
    C = [C ; C(:,2) C(:,1)];
%     permu = perms(1:num_shapes);
%     C = permu(:,1:num_conditions);
%     C = unique(C, 'rows');
    
    for i=1:size(C,1)
        
        cur_shape = zeros(num_conditions*num_timebins, 1);
        
        for cond_i=1:num_conditions
                                    
            cur_shape(((cond_i-1)*num_timebins+1):(cond_i*num_timebins), :) = shapes{C(i, cond_i)};
            
        end
        
        cur_shape = repmat(cur_shape, num_subjects, 1);
        
        target_shapes(:,(num_shapes-1)+i) = cur_shape;
        
    end
    
end

end
