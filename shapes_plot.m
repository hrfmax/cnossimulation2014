function [fig]=shapes_plot(simulation_name, ni, it, p_model, p, num_timebins, num_conditions, num_components, label)
% SHAPES_PLOT plots single shapes for specified combination of simulation
% parameters. This is neccessary for the automatic production of reports. 
%
% @author Konstantin Helmsauer, Jan Boelts
% {konstantinhelmsauer}{j.f.boelts}@gmail.com

num_subjects = (size(p_model, 1)/num_timebins)/num_conditions;

condition_linestyles = {'r', 'b', 'g'};

fig=figure;
set(fig, 'Name', ['model vs. ' label])

x = zeros(num_components, num_conditions, num_timebins);
m = zeros(num_components, num_conditions, num_timebins);

cor_s = stats(p_model, p, 'spearman');
cor_p = stats(p_model, p, 'pearson');

for comp = 1:num_components
    for cond=1:num_conditions
        for subj = 1:num_subjects
            for tb = 1:num_timebins
                row = (subj-1)*num_conditions*num_timebins + (cond-1)*num_timebins + tb;
                x(comp, cond, tb) = x(comp, cond, tb) + p(row, comp);
            end
        end
    end
end

x = x./(num_subjects);

for comp = 1:num_components
    for cond=1:num_conditions
        for subj = 1:num_subjects
            for tb = 1:num_timebins
                row = (subj-1)*num_conditions*num_timebins + (cond-1)*num_timebins + tb;
                m(comp, cond, tb) = x(comp, cond, tb) + p_model(row, comp);
            end
        end
    end
end

m = m./(num_subjects);

for comp=1:num_components
    
    subplot(1,num_components,comp)
    
    for cond=1:num_conditions
        
        mp = reshape(m(comp, cond, 1:num_timebins), 1, num_timebins);
        xp = reshape(x(comp, cond, 1:num_timebins), 1, num_timebins);
        plot(mp, [condition_linestyles{cond} '-'])
        hold on
        plot(xp, [condition_linestyles{cond} '--'])
        hold on
    
    end
    
end

title(['SpearmanMedian: ' num2str(cor_s) ' PearsonMedian: ' num2str(cor_p)])

legend('Model', label)

saveas(fig, ['results/' simulation_name '/shapes_n' num2str(ni) '_i' num2str(it) '_' label '.fig']);

end