function []=iterationstep(simulation_name, ni, it)
%ITERATIONSTEP performs one iteration 
% This function performs one iteration step of the monte carlo simulation
% of the different rotation methods. 
% INPUT 
%   simulation_name     the filename of a matlab file containing parameters
%                       needed for the current iteration 
%   ni                  index for the level of induced noise
%   it                  index of the current iteration
% 
% The MATFILE in simulation_name loads the following variables into the
% workspace: 
% TR                    time repitition of the simulated fMRI, e.g., 2
% hrfmax_iterations     iterations used for the naive (old) hrfmax rotation
% iterations            # of total iterations
% noice_perc            array of amounts of noise
% num_components        number of components used in the current iteration
% num_conditions        number of conditions used in the current iteration
% num_repetitions       number of trials per conditions used in the current iteration
% num_subjects          number of subjects used in the current iteration
% num_timebins          number of timebins used in the current iteration
% savedata              flag for saving the results
%
% @author Konstantin Helmsauer, Jan Boelts
% {konstantinhelmsauer}{j.f.boelts}@gmail.com

%% START
%%%%%%%%%%%%%%%%%%%%%%%%%%%%% PREPARE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% clean workspace
clc; close all;

% cast the iteration and noise variables to integers
%ni = str2num(ni);
%it = str2num(it);

% set seeds of random number generator
seed = int32((ni-1)*200 + it);
rng(seed);

% load simulation parameters
load(['./job_files/' simulation_name]);

%% %%%%%%%%%%%%%%%%%%%%% Set Simulation Parameters %%%%%%%%%%%%%%%%%%%%%%%%

% load original V matrix from Working Memory study. It refers to the V
% matrix resulting from SVD. It represents the brain activations. In general 
% rotation methods should maintain the structure of the brain networks.
% Here, we use this original V to quantify to what extend the new rotation
% methods change the underlying brain activations. 

% the v_mode decides whether to load an original V matrix from some study
% or to just use a random V matrix. We use a random V matrix in some cases
% because rotation methods purely focused on optimizing simple strucutr 
% (e.g., Varimax) may have disadvantage if we only use brain data. 
if strcmp(v_mode,'random')
    V=randn(29151,num_components); % produce random brain activations
else
    % or load original brain activations
    load(['V_matrices/V_' v_mode '_' num2str(num_components) 'c.mat']);
end

% check whether the number of components in this iteration matches the
% columns in the V matrix. 
if size(V,2) ~= num_components
    error('Wrong V Matrix Column Number');
end

% In this simulation all scans are modeled by the CPCA. Therefore the total 
% number of scans is just the number of timebins * the number of conditions 
% * the number of trials/repetitions per subject: 
% is unused, commented out by Jan, 01.11.2014
% num_scans = num_conditions*num_timebins*num_repetitions; 

% for each iteration different level of gaussian noise is induced into the 
% artificial brain data. Here we just take the current level of noise
% (there are 5) using the idx given as argument in the function. 
noise = noise_perc(ni);

%% %%%%%%%%%%%%%%%%%%%%%%%% BEGIN SIMULATION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% get events for this iteration. They are randomly chosen from a
% pool of plausible events. The events variable is a cell array with
% conditions * components cells, containing the onset and the durations of
% the events. 
events = choose_events(num_conditions, num_components,seed);

% now create the p model using the new events. p_model will contain the
% ideal p-matrix with pure hrf-shapes in each component and condition. 
p_model = get_p_model(num_subjects, num_timebins, TR, events, 0);
% p_model = randn(size(p_model,1), size(p_model,2)); % ------------------------------

% Now create a set of guessed events that will be used by the new hrf
% procrustes rotation method for rotating to the underlying true shapes
% (those inside the event cell array): guessed shapes contains all the 
% induced events plus 2 distractors
% transpose for easier processing
tempevents = events';
if num_conditions==1 
    % if there is only one condition take whole event array plus distractors
    guessed_events = [events{:}, {[0 2]}, {[4 2]}];
else
    % else: take only unique events from the cell array, plus two distractors
    guessed_events = [tempevents{((num_components/2)+1):end}, {[0 2]}, {[4 2]}];
end

% get resulting number of guessed events
num_guessed_events = length(guessed_events);

% Compute all combinations of putting all guessed events on every
% component and condition
% The target_shapes matrix contains all the components/columns that we will
% try out during rotation
target_shapes = get_target_shapes(num_subjects, num_timebins, num_conditions, TR, guessed_events);

% Some helping code for combinations for procrustes and hippocrustes
% compute all possible combinations of ordering shapes in components: 
% having all the possible target components we have to find all
% combinations of choosing #component many from this set. 
combos      = nchoosek(1:size(target_shapes,2), num_components);
% combos_lite ignores the different shapes per components case
combos_lite = nchoosek(1:num_guessed_events,num_components);

% Create a dummy coded G Matrix: (num_subjects * num_scans) by (num_subj * num_timebins * num_conditions)
G = createG(num_subjects, num_conditions, num_repetitions, num_timebins);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% CREATE THE MODEL %%%%%%%%%%%%%%%%%%%%%%%%%%%

% We now have the p_model matrix which represents the P matrix in the CPCA
% and contains several components (columns) with repeated HRF shapes for
% each condition. 
% In addition we have the dummy coded G matrix. 
% From CPCA we know that P represents the weights that would be applied to
% G to create U (the matrix of singular vectors from SVD, the components):
% U = G x P. Here, we have G and we have P, so we can create U: 
% Create U from G and P
U = G*p_model;
% normalize the component matrix U (why?) 
U = U - repmat(mean(U), size(U,1), 1);

% Simulation of U and V: U contains the pure induced shapes on the
% timebins chosen for inducing the activity. By multiplication with
% V, which contains realworld component loadings, the activity in U is
% "distributed" over all scans and voxels in V.

% build the model using U and V
M = U*V';

% The model M contains the underlying HRF shapes we chose hidden through 
% the multiplication with V. We now apply SVD and try to extract the HRFs 
% in U using different rotation methods. 

% THE FOLLOWING NORMALIZATION STEPS WERE CHANGED SEVERAL TIMES
% normalize each subject block in UV matrix 
%-----for i=1:num_scans:(size(G,1)-num_scans+1)
%-----rows = i:i+(num_scans-1);
%-----M(rows,:) = (M(rows,:) - repmat(mean(M(rows,:)),num_scans,1))./repmat(std(M(rows,:)),num_scans,1);
%-----end

% add different noise for each iteration
%UV_noisy = (1-noise)*M + noise*randn(size(M,1), size(M,2));
%noiseM = ones(size(M,1), size(M,2)) * mean(M(:)) + (randn(size(M,1), size(M,2))*std(M(:)));
%UV_noisy = (1-noise)*M+noise*noiseM;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ADD NOISE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% WE NOW GO WITH THIS VERSION: 01.11.2014 by Jan
% To the generated model M we will add noise to have a more realistic
% situation. The level of noise is specified in the simulation parameters

% determine the standard deviation of the gaussian noise, just take the
% maximum std from the model M. 
noise_std = max(std(M)); 

% Mz = zscore(M); % this was unused and commented out (01.11.2014, Jan) 

% Add the noise to the model, use gaussian random noise (Matlab randn) with
% changed standard deviation
UV_noisy = (1-noise)*M + noise*randn(size(M,1), size(M,2))*noise_std; 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% DO SVD %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% We apply SVD on the noisy model UV_noisy and try to get back the p_model
% we used to build U. 

% Apply SVD on UV, directly extract the needed numder of components
% UV_noisy has full rank! (Jan, 01.11.2014) 
[U_unrot, ~, V_unrot] = svds(UV_noisy, num_components);

% compute p_unrot from G invers and U
p_unrot = pinv(G)*U_unrot;
% This p_unrot matrix to some extend contains the hrf shapes induced using
% the self-made p_model matrix, G and U. Now we use different rotation
% methods to get a rotated p matrix that is as similar to p_model as
% possible. 

% VD_unrot = V_unrot*D_unrot; % VD_unrot is not needed. THere was some
% confusion on wheter to use V or VD for further processing. Look the paper
% Metzak et al. 2009. in Human Brain Mapping. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ROTATION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%% VARIMAX ON V %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% do varimax rotation with Kaiser normalization
[V_Varim, T_varim] = rotatefactors(V_unrot,'Method','varimax','Normalize','on','Maxit',5000);

% compute weighted V
% VD_varim = VD_unrot*T_varim;

% apply obtained transformation matrix to U and to p_unrot
p_varim = p_unrot*T_varim;

%%%%%%%%%%%%%%%%%%%%%%%%%%%% QUARTIMAX ON V %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% the quartimax algorithm resulted in errors or memory issued frequently,
% in those cases we used the varimax rotation results instead
try 
    [V_quartimax, T_quartimax] = rotatefactors(V_unrot, 'Method', 'quartimax','Maxit',5000);
catch
    % if not able to terminate take varimax results instead
    V_quartimax = V_unrot; % not needed 
    T_quartimax = eye(num_components);
end

% apply obtained transformation matrix to U and to p_unrot
p_quartimax = p_unrot*T_quartimax;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%% PROMAX ON V %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% similar issues as with quartimax, use varimax results if needed
try
    [~, T_promax] = rotatefactors(V_unrot, 'Method', 'promax','Maxit',5000);
catch
    V_promax = V_unrot;
    T_promax = eye(num_components);
end

% apply obtained transformation matrix to U and to p_unrot
p_promax = p_unrot*T_promax;

%%%%%%%%%%%%%%%%% HRFMAX (as in current CPCA version) %%%%%%%%%%%%%%%%%%%%%

% The old hrfmax rotation method allows for only one shape per component.
% Therefore we just take the first #guessed_events fromt the previously
% generated target_shape matrix. We only need the first #timebins rows. And
% we need to transpose because the function needs the shapes as row vectors 
hrfmax_shapes = (target_shapes(1:num_timebins, 1:num_guessed_events))';

% get the rotation matrix from hrfmax 
T_hrfmax = hrfmax(p_unrot, U_unrot, hrfmax_shapes, hrfmax_iterations);

% rotate the SVD matrices
p_hrfmax = p_unrot*T_hrfmax;
V_hrfmax = V_unrot*T_hrfmax;

%%%%%%%%%%%%%%%% ORTHOGONAL AND OBLIQUE PROCRUSTES ROTATION %%%%%%%%%%%%%%%

% prelocate p_procr and p_obl for case components 6 and 8, commented by Jan
% (01.11.2014)
% p_procr = zeros(size(p_model,1),size(p_model,2));
% p_obl = zeros(size(p_model,1),size(p_model,2));
% V_procr  = V_unrot;
% V_obl    = V_unrot;

[p_procr, T_procr, combo_procr, p_obl, T_obl, combo_obl] = my_procrustes(p_unrot, target_shapes, combos);

%         VD_procr = VD_unrot*T_procr;
%         VD_obl   = VD_unrot*T_obl;
V_procr  = V_unrot*T_procr;
V_obl    = V_unrot*T_obl;

%%%%%%%%%%%%%%%%%%%%%%%%%%% PROCRUSTES LITE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Lite version of Procrustes Rotation on P: 
% like HRFMAX allow only 1 shape per component, regardless of condition
[p_plite, T_plite, combo_plite, p_olite, T_olite, combo_olite]=my_procrustes(p_unrot,target_shapes(:,1:length(guessed_events)),combos_lite);
%         VD_plite = VD_unrot*T_plite;
%         VD_olite = VD_unrot*T_olite;
V_plite  = V_unrot*T_plite;
V_olite  = V_unrot*T_olite;


%%%%%%%%%%%%%%%%%%% Random rotation as baseline %%%%%%%%%%%%%%%%%%%%%%%%%%%
% rotate U, P, and VD with same random, but orthogonal rotation matrix.
T_random = orth(rand(num_components));

p_random  = p_unrot*T_random;
V_random  = V_unrot*T_random;
%         VD_random = VD_unrot*T_random;

% Changed by Jan, 01.11.2014. I do not know what exactly we planned to do
% here but I will change it in a way that makes sense to me today! 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% EVALUATION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% take all rotation methods
methods = {'unrot', 'random', 'varim', 'plite', 'procr', 'obl'}; 

% for each method 
for cur_meth = 1:length(methods)
    
    % save the resulting p-matrix
    eval(['cur_p = p_' methods{cur_meth} ';']);
    
    % calculate the possible combinations of ordering the components. This
    % is done because the order of components from SVD is random and the
    % columns in the rotated p matrices may correspond to different columns
    % in the original p_model matrix. 
    all_perms = perms(1:num_components);
    % prelocate the correlation coeffs
    best_cor = 0;
    best_perm = 1;
    
    % for each permuation of columns in P 
    for thisperm = 1:length(all_perms)
        % take the current permutation of columns as a p
        cur_perm_p = cur_p(:,all_perms(thisperm,:));
        % check whether the median of the correlations of the columns of
        % p_model and p_rotated is higher than the best one
        % Here, I added the absolute value function because the sign of
        % correlation does not matter (Jan, 01.11.2014). 
        if median(abs(diag(corr(p_model, cur_perm_p)))) > best_cor 
            % if yes save it
            best_perm = thisperm;
            best_cor = median(abs(diag(corr(p_model, cur_perm_p))));
        end
    end
    
    % proceed with the permutation that yielded the best fit
    cur_p = cur_p(:,all_perms(best_perm,:));
    
    % SADLY I HAVE NO IDEA WHAT WE ARE DOING HERE; TO ME IT SEEMS WRONG! 
    for thiscomp = 1:num_components
        
        cur_p(:,thiscomp) = (cur_p(:,thiscomp)./norm(cur_p(:,thiscomp))).*norm(p_model(:,thiscomp));
        
        
        if corr(p_model(:,thiscomp), cur_p(:,thiscomp)) < 0 
            cur_p(:, thiscomp) = -cur_p(:,thiscomp);
        end
        
    end
    
    %shapes_plot(simulation_name, ni, it, p_model, cur_p, num_timebins, num_conditions, num_components, methods{cur_meth});
    %waitforbuttonpress
    close all

end

clear combos target_shapes
save(['simulations/' simulation_name '/n' num2str(ni) '_i' num2str(it) '.mat']);

end