function [ tcc ] = totalcongrcoef(A,B)
% Calculates the total coefficient of congruence between
% two equally sized matrices A and B
% In contrast to the congrcoeff.m function, this function this function takes whole
% matrices as arguments (not single columns). 
% 
% @author Konstantin Helmsauer, Jan Boelts
% {konstantinhelmsauer}{j.f.boelts}@gmail.com

tcc = trace(A'*B) / (sqrt(trace(A'*A))*sqrt(trace(B'*B)));

end

