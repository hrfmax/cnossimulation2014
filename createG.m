function [G] = createG(num_subjects, num_conditions, num_repetitions, num_timebins)
% CREATEG creates a dummy coded G matrix for the linear regression of CPCA
% This function creates a dummy coded design matrix with dimension
% corresponding to the number of subjects, conditions, trials and timebins.
% The design matrix contains only 0 and 1 and will be used as a dummy coded
% model for the linear regression in CPCA
% INPUT 
%   num_subjects        number of subjects
%   num_conditions      number of conditions
%   num_repetitions     number of trials per subject
%   num_timebins        number of timebins modeled
% OUTPUT 
%   G                   the dummy coded design matrix 
%
% @author Konstantin Helmsauer, Jan Boelts
% {konstantinhelmsauer}{j.f.boelts}@gmail.com

% Create a partial G Matrix for only one Subject and one Condition. It
% contains ones on the diagonal so that each scan is modeled, there are
% exactly #timebins scans per trial/subject. 
% the identity matrices are concatenated vertically #repetitions times
% (once for each trial) 
SubjectG_OneCondition = repmat(eye(num_timebins, num_timebins),num_repetitions,1);

% get dimensions
SubjectG_OneCondition_Rows = size(SubjectG_OneCondition, 1);
SubjectG_OneCondition_Cols = size(SubjectG_OneCondition, 2);

% Prelocate the partial G Matrix for only one Subject but all Conditions
SubjectG = zeros(num_conditions*SubjectG_OneCondition_Rows,num_conditions*SubjectG_OneCondition_Cols);

% now fill up the empty parts with dummy code
for cond = 1:num_conditions
    % find the current indices
    row = (cond*SubjectG_OneCondition_Rows-(SubjectG_OneCondition_Rows-1)):(cond*SubjectG_OneCondition_Rows);
    col = (cond*SubjectG_OneCondition_Cols-(SubjectG_OneCondition_Cols-1)):cond*SubjectG_OneCondition_Cols;
    % put in the one-subject-one-condition G matrix template
    SubjectG(row,col)=SubjectG_OneCondition;
end
% now we are done for one subject. get the new dimensions: 
SubjectG_Rows = size(SubjectG, 1);
SubjectG_Cols = size(SubjectG, 2);

% Prelocate the G Matrix for all Subjects
G = zeros(num_subjects*SubjectG_Rows,num_subjects*SubjectG_Cols);

% fill up the empty parts for each subject 
for subj = 1:num_subjects 
    % get indices
    row = (subj*SubjectG_Rows-(SubjectG_Rows-1)):(subj*SubjectG_Rows);
    col = (subj*SubjectG_Cols-(SubjectG_Cols-1)):subj*SubjectG_Cols;
    % put in the one-subject-ALL-conditions G matrix template
    G(row,col)=SubjectG;
end

end

