function [str]=num2word(n)
% For visualization purposes
%
% @author Konstantin Helmsauer, Jan Boelts
% {konstantinhelmsauer}{j.f.boelts}@gmail.com
switch n
    case 0
        str = 'zero';
    case 1
        str = 'one';
    case 2
        str = 'two';
    case 3
        str = 'three';
    case 4
        str = 'four';
    case 5
        str = 'five';
    case 6
        str = 'six';
    case 7
        str = 'seven';
    otherwise
        str = 'notparsable'
end

end